insert  into course(id, name, updated_date, created_date,is_deleted) 
values (10001,'java',sysdate(), sysdate(),false),
(10002,'html/css',sysdate(), sysdate(),false),
(10003,'cpp',sysdate(), sysdate(),false),
(10004,'python',sysdate(), sysdate(),false);

insert into Passport(id, number)
values(40001,'A10001U'),(40002,'B10001V'),(40003,'C10001X');

insert into Student(id, name, passport_id)
values(20001,'besart',40001),(20002,'bleron',40002),(20003,'shpend',40003);


insert into Review(id, rating, description,course_id)
values(50001,'THREE', 'Useful',10001),(50002,'FOUR','Nice',10001),(50003,'FIVE','Very good',10003);


insert into student_course(student_id, course_id)
values(20001,10001),(20001,10003),(20003,10001),(20002,10004),(20002,10003)
		