package com.shpend.demo.repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

//import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.shpend.demo.entity.Course;
import com.shpend.demo.entity.Review;
import com.shpend.demo.entity.ReviewRating;

@Repository
@Transactional
public class CourseRepository {
	
	@Autowired
	EntityManager em;
	
	public Course findById(long id) {
		return em.find(Course.class, id);
	}
	
	
	public boolean deleteById(long id) {
		Course course = findById(id);
		em.remove(course);
		if(course == null)
			return false;
		return true;
	}
	
	public Course save(Course course) {
		if(findById(course.getId()) == null)
			 em.persist(course);
		else
			return em.merge(course);
		return course;
		
	}
	
	public void playEm() {
		Course course = new Course("php not updated");
		Course course2 = new Course("js not updated");
		em.persist(course);
		em.persist(course2);
		em.flush();
		course.setName("php updated");
		course2.setName("js updated");
		em.flush();
	}
	
	public void addReviews() {
		Course course1 = findById(10003l);
		Course course2 = findById(10004l);
		Review review1 = new Review(ReviewRating.FIVE,"Great course indeed");
		Review review2 = new Review(ReviewRating.THREE,"Could be better");
		
		
		
		course1.addReview(review1);
		review1.setCourse(course1);
		
		course2.addReview(review2);
		review2.setCourse(course2);
		
		
		em.persist(review1);
		em.persist(review2);
		
	}
	
}
