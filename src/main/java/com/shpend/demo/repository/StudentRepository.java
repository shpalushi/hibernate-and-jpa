package com.shpend.demo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

//import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.shpend.demo.entity.Course;
import com.shpend.demo.entity.Passport;
import com.shpend.demo.entity.Student;

@Repository
@Transactional
public class StudentRepository {
	
	@Autowired
	EntityManager em;
	
	public Student findById(long id) {
		return em.find(Student.class, id);
	}
	
	public boolean deleteById(long id) {
		Student student = findById(id);
		em.remove(student);
		if(student == null)
			return false;
		return true;
	}
	
	public Student save(Student student) {
		if(findById(student.getId()) == null)
			 em.persist(student);
		else
			return em.merge(student);
		return student;
		
	}
	
	public void playEm() {
		Student student = new Student("php not updated");
		Student student2 = new Student("js not updated");
		em.persist(student);
		em.persist(student2);
		em.flush();
		student.setName("php updated");
		student2.setName("js updated");
		em.flush();
	}
	public void saveStudentWithPassport(String studentName, String passportNumber) {
		Passport passport = new Passport(passportNumber);
		em.persist(passport);
		Student student = new Student(studentName);
		student.setPassport(passport);
		em.persist(student);
	}
	
//	@Transactional
	public List<Course> retrieveStudentAndCourses(int id) {
		Student student = findById(id);
		return student.getCourses();
	}
	
	public void addStudentAndCourse(Student student, Course course) {
		em.persist(student);
		em.persist(course);
		student.addCourse(course);
		course.addStudent(student);
		em.persist(student);
	}
	
}
