package com.shpend.demo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

//import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.shpend.demo.entity.Course;
import com.shpend.demo.entity.Employee;
import com.shpend.demo.entity.FullTimeEmployee;
import com.shpend.demo.entity.PartTimeEmployee;
import com.shpend.demo.entity.Review;

@Repository
@Transactional
public class EmployeeRepository {
	
	@Autowired
	EntityManager em;
	
	public void insert(Employee employee) {
		em.persist(employee);
	}
	
	public List<PartTimeEmployee> retrieveAllPartTimeEmployees() {
		TypedQuery<PartTimeEmployee> query = em.createQuery("Select e from PartTimeEmployee e",PartTimeEmployee.class);
		List<PartTimeEmployee> list = query.getResultList();
		return list;
	}
	
	
	public List<FullTimeEmployee> retrieveAllFullTimeEmployees() {
		TypedQuery<FullTimeEmployee> query = em.createQuery("Select e from FullTimeEmployee e",FullTimeEmployee.class);
		List<FullTimeEmployee> list = query.getResultList();
		return list;
	}
	
}
