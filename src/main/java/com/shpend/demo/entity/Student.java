package com.shpend.demo.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
//@NamedQueries(value={@NamedQuery(name="select_all_query", query="select c from Course c"),
//					@NamedQuery(name="select_shpend", query="Select c from Course c where name like 'shpendp'")
//})
//@Table(name = "CourseDetails")
public class Student {
	
	@Id
	@GeneratedValue
	private long id;
	//@Column(name = "fullname", nullable = false)
	private String name;
	
	@Embedded
	private Adress adress;
	

	@OneToOne(fetch=FetchType.LAZY)
	private Passport passport;
	
	@ManyToMany(fetch= FetchType.EAGER)
	//join column - STUDENT_ID
	//inverse join column - COURSE_ID
	@JoinTable(name="STUDENT_COURSE",
				joinColumns=@JoinColumn(name="STUDENT_ID"),
				inverseJoinColumns=@JoinColumn(name="COURSE_ID"))
	private List<Course> courses = new ArrayList<>();
//	@UpdateTimestamp
//	private LocalDateTime updatedDate;
//	
//	@CreationTimestamp
//	private LocalDateTime createdDate;
	

	public Adress getAdress() {
		return adress;
	}

	public void setAdress(Adress adress) {
		this.adress = adress;
	}
	
	public Passport getPassport() {
		return passport;
	}

	public void setPassport(Passport passport) {
		this.passport = passport;
	}

	public Student() {
		
	}
	
	public Student(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public long getId() {
		return this.id;
	}
	
	

	public List<Course> getCourses() {
		return courses;
	}

	public void addCourse(Course course) {
		this.courses.add(course);
	}

	@Override
	public String toString() {
		return "Student [name=" + name + "]";
	}
	
	
}
