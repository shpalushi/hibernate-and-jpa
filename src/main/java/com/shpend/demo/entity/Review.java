package com.shpend.demo.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
//@NamedQueries(value={@NamedQuery(name="select_all_query", query="select c from Course c"),
//					@NamedQuery(name="select_shpend", query="Select c from Course c where name like 'shpendp'")
//})
//@Table(name = "CourseDetails")
public class Review {
	
	@Id
	@GeneratedValue
	private long id;
//	@Column(nullable = false)
	
	@Enumerated(value=EnumType.STRING)
	private ReviewRating rating;
	
	private String description;
	
	@ManyToOne
	private Course course;
	
	
	
//	@UpdateTimestamp
//	private LocalDateTime updatedDate;
//	
//	@CreationTimestamp
//	private LocalDateTime createdDate;
	


	public Review() {
		
	}
	
	public Review(ReviewRating rating, String description) {
		this.description = description;
		this.rating = rating;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	public long getId() {
		return this.id;
	}

	public ReviewRating getRating() {
		return rating;
	}

	public void setRating(ReviewRating rating) {
		this.rating = rating;
	}
	
	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	} 	 

	@Override	
	public String toString() {
		return "Review [description=" + description + ", rating"+ rating +"]";
	}
	
	
}
