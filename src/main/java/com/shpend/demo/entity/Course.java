package com.shpend.demo.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@NamedQueries(value={@NamedQuery(name="select_all_query", query="select c from Course c"),
					@NamedQuery(name="select_shpend", query="Select c from Course c where name like 'shpendp'"),
					@NamedQuery(name="select_all_query_join", query="select c from Course c JOIN FETCH c.students s")
}) 
//@Table(name = "CourseDetails")
@Cacheable
@SQLDelete(sql="update course set is_deleted=true where id=?")
@Where(clause="is_deleted = false")
public class Course {
	
	@Id
	@GeneratedValue
	private long id;
	//@Column(name = "fullname", nullable = false)
	private String name;
	
	@UpdateTimestamp
	private LocalDateTime updatedDate;
	
	@CreationTimestamp
	private LocalDateTime createdDate;
	
	private boolean isDeleted;
	
	@OneToMany(mappedBy="course")
	private List<Review> reviews;
	
	@ManyToMany(mappedBy="courses")
	@JsonIgnore
	private List<Student> students = new ArrayList<>();
	
	@PreRemove
	private void preRemove() {
		this.isDeleted = true;
	}
	
	public Course() {
		
	}
	
	public Course(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public long getId() {
		return this.id;
	}
	
	public List<Review> getReviews() {
		return reviews;
	}

	public void addReview(Review review) {
		this.reviews.add(review);
	}

	public void removeReview(Review review) {
		this.reviews.remove(review);
	}
	
	
	public List<Student> getStudents() {
		return students;
	}

	public void addStudent(Student student) {
		this.students.add(student);
	}

	@Override
	public String toString() {
		return "Course [name=" + name + "]";
	}
	
	
}
