package com.shpend.demo.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
//@NamedQueries(value={@NamedQuery(name="select_all_query", query="select c from Course c"),
//					@NamedQuery(name="select_shpend", query="Select c from Course c where name like 'shpendp'")
//})
//@Table(name = "CourseDetails")
public class Passport {
	
	@Id
	@GeneratedValue
	private long id;
	@Column(nullable = false)
	private String number;
	@OneToOne(fetch=FetchType.LAZY, mappedBy="passport")
	private Student student;
	
//	@UpdateTimestamp
//	private LocalDateTime updatedDate;
//	
//	@CreationTimestamp
//	private LocalDateTime createdDate;
	
	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Passport() {
		
	}
	
	public Passport(String number) {
		this.number = number;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	public long getId() {
		return this.id;
	}

	@Override	
	public String toString() {
		return "Passport [number=" + number + "]";
	}
	
	
}
