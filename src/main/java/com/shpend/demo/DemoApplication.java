package com.shpend.demo;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.shpend.demo.entity.Course;
import com.shpend.demo.entity.Student;
import com.shpend.demo.repository.CourseRepository;
import com.shpend.demo.repository.EmployeeRepository;
import com.shpend.demo.repository.StudentRepository;
import com.shpend.demo.entity.FullTimeEmployee;
import com.shpend.demo.entity.PartTimeEmployee;

@SpringBootApplication
public class DemoApplication  implements CommandLineRunner{
	@Autowired
	EntityManager em;
	@Autowired
	CourseRepository courseRepository;
	
	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	public static void main(String[] args)  {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("Found user->{}", courseRepository.findById(10002L));
		//repository.deleteById(10003);
		//logger.info("Deleted user state ->{}", repository.deleteById(10003));
		//logger.info("Inserted user with credentials -? {}", courseRepository.save(new Course("triim")));
		
		courseRepository.playEm();
		
		TypedQuery<Course> q = em.createNamedQuery("select_shpend", Course.class);
		List<Course> l1 = q.getResultList();
		TypedQuery<Course> query = em.createNamedQuery("select_all_query", Course.class);
		List<Course> list = query.getResultList();
		logger.info("Select c from course c ->{}", list);
		logger.info("Shpend name contents->{}", l1);
		
		studentRepository.saveStudentWithPassport("ShpendBosi", "D20002V");	
		courseRepository.addReviews();
		
		logger.info("courses by student with id 20001 ->{}",studentRepository.retrieveStudentAndCourses(20001));
		
		studentRepository.addStudentAndCourse(new Student("Kreshnik"), new Course("Rust for backend"));
		
		employeeRepository.insert(
					new FullTimeEmployee("Ben",new BigDecimal("10000")));
		employeeRepository.insert(
				new PartTimeEmployee("Andy",new BigDecimal("50")));
		logger.info("Part Time  employees->{}", employeeRepository.retrieveAllPartTimeEmployees());
		logger.info("Full Time  employees->{}", employeeRepository.retrieveAllFullTimeEmployees());
	}
	
	public void retrieveIdAndPassport() {
		Student student = em.find(Student.class,20001);
		logger.info("Student info->{}", student);
		logger.info("Passport->{}", student.getPassport());
	}
	
}
