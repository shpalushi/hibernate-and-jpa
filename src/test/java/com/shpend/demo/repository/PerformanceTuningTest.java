package com.shpend.demo.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.Subgraph;

import org.hibernate.graph.SubGraph;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.shpend.demo.DemoApplication;
import com.shpend.demo.entity.Course;


@RunWith(SpringRunner.class)
@SpringBootTest(classes=DemoApplication.class)
class PerformanceTuningTest {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	CourseRepository repository;
	@Autowired
	EntityManager em;
	
	@Test
	@Transactional
	public void creatingNPlusOneProblems() {
		List<Course> courses =em
								.createNamedQuery("select_all_query",Course.class)
								.getResultList();
		for(Course course : courses) {
			logger.info("Course -> {},      Students->{}", course, course.getStudents());
		}
	}
	
	@Test
	@Transactional
	public void solveOnePlusOneProblem_graph() {
		EntityGraph<Course> entityGraph = em.createEntityGraph(Course.class);
		Subgraph<Object> subGraph = entityGraph.addSubgraph("students");
		List<Course> courses =em
								.createNamedQuery("select_all_query",Course.class)
								.setHint("javax.persistence.loadgraph", entityGraph)
								.getResultList();
		for(Course course : courses) {
			logger.info("Course -> {},      Students->{}", course, course.getStudents());
		}
	}
	
	
	@Test
	@Transactional
	public void solveOnePlusOneProblem_joinFetch() {
		List<Course> courses =em
				.createNamedQuery("select_all_query_join",Course.class)
				.getResultList();
for(Course course : courses) {
logger.info("Course -> {},      Students->{}", course, course.getStudents());
}
	}
	
}
