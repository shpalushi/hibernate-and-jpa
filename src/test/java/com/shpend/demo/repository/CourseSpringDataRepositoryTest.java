package com.shpend.demo.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
//import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.shpend.demo.DemoApplication;
import com.shpend.demo.entity.Course;


@RunWith(SpringRunner.class)
@SpringBootTest(classes=DemoApplication.class)
class CourseSpringDataRepositoryTest {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	CourseSpringDataRepository repository;
	
	@Test
	public void findByIdCoursePresent() {
		Optional<Course> courseOptional = repository.findById(10001L);
		assertTrue(courseOptional.isPresent());
	}
	
	@Test
	public void findByIdCourseNotPresent() {
		Optional<Course> courseOptional = repository.findById(20001L);
		assertFalse(courseOptional.isPresent());
	}
	
	@Test
	public void playAroundWithSpringJpaRepository() {
		Course course = new Course("Haskell");
		repository.save(course);
		course.setName("Haskell - updated");
		repository.save(course);
	}
	
	@Test
	public void sort() {
//		Sort sort = new Sort(Sort.Direction.ASC,"name");
//		logger.info("Sorted courses -> {}", repository.findAll(sort));
	}
	
	@Test
	public void paggination() {
		PageRequest pageRequest = PageRequest.of(0, 3);
		Page<Course> firstPage = repository.findAll(pageRequest);
		logger.info("Paged courses -> {}",firstPage.getContent()) ;
		
		Pageable secondPageable = firstPage.nextPageable();
		Page<Course> secondPage = repository.findAll(secondPageable);
		logger.info("Paged courses -> {}",secondPage.getContent());
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
