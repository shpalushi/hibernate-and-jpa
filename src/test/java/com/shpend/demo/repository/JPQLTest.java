package com.shpend.demo.repository;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

import com.shpend.demo.DemoApplication;
import com.shpend.demo.entity.Course;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=DemoApplication.class)
public class JPQLTest {
	
	@Autowired
	EntityManager em;
	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Test
	public void makingJPQL() {
		TypedQuery<Course> q = em.createNamedQuery("select_shpend", Course.class);
		List<Course> l1 = q.getResultList();
		TypedQuery<Course> query = em.createNamedQuery("select_all_query", Course.class);
		List<Course> list = query.getResultList();
		logger.info("Select c from course c ->{}", list);
		logger.info("Shpend name contents->{}", l1);
	}
	@Test
	public void select_courses_without_students() {
		TypedQuery<Course> query = em.createQuery("Select c from Course c order by size (c.students) desc", Course.class);
		List<Course> list = query.getResultList();
		logger.info("Results->{}", list);
	}
	
	
	@Test
	public void join() {
		Query query = em.createQuery("select c, s from Course c JOIN c.students s");
		List<Object[]> resultList = query.getResultList();
		logger.info("JOIN");
		logger.info("Result size ->{}", resultList.size());
		for(Object[] object : resultList) {
			logger.info("course->{}, student->{}",object[0],object[1] );
		} 
	}
	
	@Test
	public void left_join() {
		Query query = em.createQuery("select c, s from Course c LEFT JOIN c.students s");
		List<Object[]> resultList = query.getResultList();
		logger.info("LEFT_JOIN");
		logger.info("Result size ->{}", resultList.size());
		for(Object[] object : resultList) {
			logger.info("course->{}, student->{}",object[0],object[1] );
		} 
	}

	@Test
	public void cross_join() {
		
		Query query = em.createQuery("select c, s from Course c, Student s");
		List<Object[]> resultList = query.getResultList();
		logger.info("CROSS_JOIN");
		logger.info("Result size ->{}", resultList.size());
		for(Object[] object : resultList) {
			logger.info("course->{}, student->{}",object[0],object[1] );
		} 
	}

	
}
