package com.shpend.demo.repository;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

import com.shpend.demo.DemoApplication;
import com.shpend.demo.entity.Course;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=DemoApplication.class)
public class CriteriaQueryTest {
	
	@Autowired
	EntityManager em;
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	@Test
	public void all_courses() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Course> cq = cb.createQuery(Course.class);
		
		Root<Course> courseRoot = cq.from(Course.class);
		//select c from Course c
		TypedQuery<Course> q = em.createQuery(cq.select(courseRoot));
		List<Course> list = q.getResultList();
		logger.info("Typed query->{}", list);
	}
	
	
	@Test
	public void like_condition_courses() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Course> cq = cb.createQuery(Course.class);
		
		Root<Course> courseRoot = cq.from(Course.class);
		
		Predicate likeJs = cb.like(courseRoot.get("name"), "%js%");
		
		cq.where(likeJs);
		//select c from Course c
		
		TypedQuery<Course> q = em.createQuery(cq.select(courseRoot));
		List<Course> list = q.getResultList();
		logger.info("like, where->{}", list);
	}
	
	@Test
	public void empty_student_courses() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Course> cq = cb.createQuery(Course.class);
		
		Root<Course> courseRoot = cq.from(Course.class);
		
		Predicate studentsIsEmpty = cb.isEmpty(courseRoot.get("students"));
		
		cq.where(studentsIsEmpty);
		//select c from Course c where c.students is empty
		
		TypedQuery<Course> q = em.createQuery(cq.select(courseRoot));
		List<Course> list = q.getResultList();
		logger.info("like, where->{}", list);
	}
	
	@Test
	public void join() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Course> cq = cb.createQuery(Course.class);
		
		Root<Course> courseRoot = cq.from(Course.class);
		
		courseRoot.join("students"); 
		//select c from Course c where c.students is empty
		
		TypedQuery<Course> q = em.createQuery(cq.select(courseRoot));
		List<Course> list = q.getResultList();
		logger.info("join->{}", list);
	}
	
	@Test
	public void left_join() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Course> cq = cb.createQuery(Course.class);
		
		Root<Course> courseRoot = cq.from(Course.class);
		
		courseRoot.join("students", JoinType.LEFT); 
		//select c from Course c where c.students is empty
		
		TypedQuery<Course> q = em.createQuery(cq.select(courseRoot));
		List<Course> list = q.getResultList();
		logger.info("join->{}", list);
	}
}
