package com.shpend.demo.repository;

import static org.junit.Assert.assertEquals;
import com.shpend.demo.entity.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import com.shpend.demo.DemoApplication;
import com.shpend.demo.entity.Course;
import com.shpend.demo.entity.Student;


@RunWith(SpringRunner.class)
@SpringBootTest(classes=DemoApplication.class)
 public class StudentRepositoryTest {

	@Autowired
	StudentRepository repository;
	@Autowired
	EntityManager em;
	@Test
	public void findByIdTest() {
		Student student= repository.findById(10002);
		assertEquals("java", student.getName());
	}
	@Test
	@DirtiesContext
	public void deleteByIdTest() {
		assertTrue(repository.deleteById(10002));
	}
	
	
	@Test
	@Transactional
	public void addStudentAdress() {
		Student student= repository.findById(20001);
		student.setAdress(new Adress("Cliffside Park","Commercial Avenue","New Jersey"));
		em.flush();
	
	}
	
	
}
