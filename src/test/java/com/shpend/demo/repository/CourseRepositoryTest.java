package com.shpend.demo.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.shpend.demo.DemoApplication;
import com.shpend.demo.entity.Course;


@RunWith(SpringRunner.class)
@SpringBootTest(classes=DemoApplication.class)
class CourseRepositoryTest {


	@Autowired
	CourseRepository repository;
	@Test
	@Transactional(isolation=Isolation.SERIALIZABLE)
	public void findByIdTest() {
		Course course = repository.findById(10002);
		assertEquals("html/css", course.getName());
		
		
	}
	@Test
	@DirtiesContext
	public void deleteByIdTest() {
		assertTrue(repository.deleteById(10002));
	}
	
	@Test
	@DirtiesContext
	public void save() {
		repository.save(new Course(null));
	}

}
