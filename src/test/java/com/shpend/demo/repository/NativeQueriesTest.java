package com.shpend.demo.repository;

//import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
//import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

import com.shpend.demo.DemoApplication;
import com.shpend.demo.entity.Course;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=DemoApplication.class)
public class NativeQueriesTest {
	
	@Autowired
	EntityManager em;
	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Test
	public void nativeQueriesBacic() {
		Query query = em.createNamedQuery("Update course set last_updated = sysdate()", Course.class);
		int noOfRowsAffected = query.executeUpdate();
		List list = query.getResultList();
		logger.info("Rows affected->{}", noOfRowsAffected);
	}

	
}
